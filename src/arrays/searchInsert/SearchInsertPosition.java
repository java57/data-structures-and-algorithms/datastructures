package arrays.searchInsert;

/**
 * This would be the straightforward solution. In worst case, complexity would be O(n).
 * However, this can be improved by binary search.
 */
public class SearchInsertPosition {
    public static void main(String[] args) {
        System.out.println(searchInsert(new int[]{1, 2, 4, 5}, 3));
    }
    public static int searchInsert(int[] nums, int target) {
        for(int i=0;i<nums.length;i++){
            if(nums[i] == target || nums[i]>target){
                return i;
            }
        }
        return nums.length;
    }
}
