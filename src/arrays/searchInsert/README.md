#### Problem Statement
```
A function that takes a sorted array and an integer.
Goal is to find a position where this interger can be placed in array.
If there is an entry in array for the input integer, return index.
```

##### Example
```

input
    [1, 2, 4, 5]
    3
output
    2

input
    [1, 2, 3, 4, 5]
    5
output
    4

input
    [1, 2, 4, 5]
    6
output
    4

```             