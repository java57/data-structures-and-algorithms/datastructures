#### Problem Statement

A function that takes two inputs, a list of blocks with the facilities in it and a list of requirement.
We need to find a most optimized location from where distance of requirements is minimum.


##### Example
```
blocks = [
    {
        "gym": false,
        "school": true,
        "store": false
    },
    {
        "gym": true,
        "school": false,
        "store": false
    },
    {
        "gym": true,
        "school": true,
        "store": false
    },
    {
        "gym": false,
        "school": true,
        "store": false
    },
    {
        "gym": false,
        "school": true,
        "store": true
    },
]
reqs = ["gym", "school", "store"]
```

**Output**

3 // at index 3, the farthest you have to walk is 1 block