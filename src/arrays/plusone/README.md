#### Problem Statement
```
A function that takes an integer array where each index has a number from 0-9.
We need to add 1 considering this array as 1 big integer and return the output array
```

##### Example
```
input = [1, 2, 3]
output = [1, 2, 4]

Explanation:
Consider 123 as a number, if we add 1 output would be 124

input = [9]
output = [1, 0]

Explanation:
Since array can have just 1 digit in each index, we need to reformat the array to accomodate new digit 

```             