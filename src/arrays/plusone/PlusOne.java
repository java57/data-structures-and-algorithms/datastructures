package arrays.plusone;

public class PlusOne {
    public static void main(String[] args) {
        int[] output = plusOne(new int[]{9});
        printArray(output);
        output = plusOne(new int[]{1, 2, 3});
        printArray(output);
        output = plusOne(new int[]{9, 9, 9});
        printArray(output);
    }
    public static int[] plusOne(int[] digits) {
        int i=digits.length-1;
        int temp = 0;
        while (i>=0){
            temp = i==digits.length-1?temp+digits[i]+1:temp+digits[i];
            if(temp>=10){
                digits[i] = temp%10;
                temp /= 10;
            }else{
                digits[i] = temp;
                temp = 0;
                break;
            }
            i--;
        }
        if(temp!=0){
            int[] newArr = new int[digits.length+1];
            System.arraycopy(digits, 1, newArr, 2, digits.length - 1);
            newArr[0] =  temp;
            return newArr;
        }
        return digits;
    }

    public static void printArray(int[] arr){
        for(int a:arr){
            System.out.print(a+", ");
        }
        System.out.println();
    }
}
