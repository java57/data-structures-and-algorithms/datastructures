package arrays.removeduplicates;

/**
 * We need to keep track of last updated index and replace the next one whenever we get a non-matching integer.
 * In this manner, we would make changes in input array only and based on index tracking we can send the size of unique numbers too.
 */
public class RemoveDuplicatesFromSortedArray {
    public static void main(String[] args) {
        int[] input = new int[]{1, 1, 2, 2, 2, 3};
        int output = removeDuplicates(input);
        for (int i=0;i<output;i++){
            System.out.print(input[i]+" ");
        }
        System.out.println();
        input = new int[]{1, 2, 3, 4};
        output = removeDuplicates(input);
        for (int i=0;i<output;i++){
            System.out.print(input[i]+" ");
        }
    }
    public static int removeDuplicates(int []input){
        if(input.length == 0){
            return 0;
        }
        if(input.length == 1){
            return 1;
        }
        int check = 0;
        for(int i=1;i<input.length;i++){
            if(input[i]!=input[i-1]){
                check+=1;
                input[check] = input[i];
            }
        }
        return check+1;
    }
}
