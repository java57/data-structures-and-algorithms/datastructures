package arrays.twonumbersum;

import java.util.HashSet;
import java.util.Set;

/***
 * I will be considering two different set of inputs.
 * 1. If array is sorted, we need to start from first and last element of input array and see if the sum is equal to n or not. If sum is less than n, then move first pointer else last.
 * 2. If array is not sorted, we would simple use hashset to calculate the sum. Iterate from first element and check if n-element is present in hash or not. If not then add existing element to hash.
 */
public class TwoNumberSum {
    public static void main(String[] args) {

        int[] sortedInput = {-1, 3, 6, 9, 10, 13};
        int[] unsortedInput = {10, -1, 9, 6, 13, 3};
        int n = 13;

        int[] output = getSumSorted(sortedInput, n);
        if(output != null){
            System.out.println("Found a pair for n as "+n+" inside sorted array: "+output[0]+", "+output[1]);
        }

        int[] output1 = getSumNonSorted(unsortedInput, n);
        if(output1 != null){
            System.out.println("Found a pair for n as "+n+" inside unsorted array: "+output1[0]+", "+output1[1]);
        }

    }

    private static int[] getSumSorted(int[] input, int n){
        if(input.length == 1){
            return null;
        }
        int i = 0, j = input.length-1;
        int tempSum;
        while(i<j){
            tempSum = input[i]+input[j];
            if(tempSum==n){
                return new int[]{input[i], input[j]};
            }
            else if(tempSum<n){
                i++;
            }
            else{
                j--;
            }
        }
        return null;
    }

    private static int[] getSumNonSorted(int[] input, int n){
        Set<Integer> numbers = new HashSet<>();
        for (int value : input) {
            if (numbers.contains(n - value)) {
                return new int[]{value, n - value};
            }
            numbers.add(value);
        }
        return null;
    }
}