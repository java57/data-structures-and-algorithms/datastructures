#### Problem Statement

A function that takes two inputs, a non-empty array and an integer n.
If any two numbers from the input array is equal to integer 'n', we need to return those two in output array.
There could be any sequence of the output.
Consider both scenarios, if the array is sorted and unsorted.


##### Example
```
array = [0, 3, 6, 8, 9, 13]
n = 12
```

**Output**

[3, 9] or [9, 3] 