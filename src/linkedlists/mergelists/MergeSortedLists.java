package linkedlists.mergelists;

import javafx.util.Pair;

/**
 * Whole idea in the solution is to change the pointers according to the values.
 * I took a new node and pointed that to different values in ascending order.
 */
public class MergeSortedLists {
    public static void main(String[] args) {

        Pair<Node, Node> ex = example1Input();
        printList(mergeNodes(ex.getKey(), ex.getValue()));
        ex = example2Input();
        printList(mergeNodes(ex.getKey(), ex.getValue()));

    }

    public static Node mergeNodes(Node n1, Node n2){
        if(n1==null && n2 == null){
            return null;
        }
        if(n1 == null){
            return n2;
        }
        if(n2 == null){
            return n1;
        }

        Node temp = new Node();
        Node head = temp;
        while (n1!=null && n2!=null){
            if(n1.value<=n2.value){
                temp.next = n1;
                n1 = n1.next;
            }
            else {
                temp.next = n2;
                n2 = n2.next;
            }
            temp = temp.next;
        }
        if(n1 != null){
            temp.next = n1;
        }
        if(n2 != null){
            temp.next = n2;
        }
        return head.next;
    }

    static class Node{
        Node next;
        int value;
        Node(){
            next = null;
        }
        Node(int value){
            this.value = value;
            next = null;
        }
    }

    public static void printList(Node head){
        while (head!=null){
            System.out.print(head.value+"->");
            head = head.next;
        }
        System.out.print("null");
        System.out.println();
    }

    public static Pair<Node, Node> example1Input(){
        Node n1 = new Node(1);
        n1.next = new Node(2);
        n1.next.next = new Node(4);

        Node n2 = new Node(1);
        n2.next = new Node(3);
        n2.next.next = new Node(5);

        return new Pair<>(n1, n2);
    }

    public static Pair<Node, Node> example2Input(){
        Node n1 = new Node(2);
        n1.next = new Node(4);
        n1.next.next = new Node(5);

        Node n2 = new Node(1);
        n2.next = new Node(3);
        n2.next.next = new Node(6);

        return new Pair<>(n1, n2);
    }
}
