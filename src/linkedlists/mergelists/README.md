#### Problem Statement
```
A function that takes two sorted list and returns a merged list in sorted order
```

##### Example
```

input
    list1: 1->2->4
    list2: 1->3->5
output = 1->1->2->3->4->5

```             