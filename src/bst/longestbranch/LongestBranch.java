package bst.longestbranch;

import java.util.ArrayList;
import java.util.List;

/***
 * To solve this problem, we need to iterate all nodes and have one check.
 * 1. If sum of left node's max depth and right node's max depth is greater than current max, then update current max with new sum
 * 2. Return current node's max depth(either left or right) to it's parent node to check first condition.
 */
public class LongestBranch {

    public static void main(String[] args) {
        Node head = createInput();
        List<Integer> output = new ArrayList<>(1);
        output.add(0, Integer.MIN_VALUE);
        getLongestDepth(head, output);
        System.out.println(output.get(0));
    }

    public static int getLongestDepth(Node node, List<Integer> output){
        if (node == null){
            return 0;
        }
        int left = getLongestDepth(node.left, output);
        int right = getLongestDepth(node.right, output);
        if(output.get(0)<(left+right)){
            output.add(0, left+right);
        }
        return Math.max(left, right)+1;
    }

    public static Node createInput(){
        Node head = new Node(1);
        head.left = new Node(2);
        head.left.left = new Node(3);
        head.left.right = new Node(4);
        head.left.left.left = new Node(5);

        head.right = new Node(6);
        head.right.left = new Node(7);
        head.right.right = new Node(8);
        head.right.left.right = new Node(9);
        return head;
    }

    static class Node{
        int value;
        Node left;
        Node right;
        Node(int value){
            this.value = value;
            left = null;
            right = null;
        }
    }

}