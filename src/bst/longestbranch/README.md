#### Problem Statement

A function which takes BST tree and returns the longest distance between any two nodes. 


##### Example
```
            10
           /  \
          /    \
         /      \
        5       15
       / \      /\
      /   \    /  \
     2    5   13  22
    /          \
   /            \
  1             14

Output: 6
Explaination: Distance in between node 1 and 14 is the longest over here.
```

```
                    3
                   / \
                  /   \
                 /     \
                2       4
                        /\
                       /  \
                      5    6
                     /      \
                    /        \
                   7          8
                  /            \
                 /              \
                9                10
                    
Output: 6
Explaination: Distance in between node 9 and 10 is the longest over here.
Also to check distance in between 2 and node 10 is 5 which is why that's not the correct answer.
```