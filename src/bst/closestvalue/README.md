#### Problem Statement

A function which takes BST tree and an integer value as inputs and returns the closest number to the integer from the tree. 


##### Example
```
            10
           /  \
          /    \
         /      \
        5       15
       / \      /\
      /   \    /  \
     2    5   13  22
    /          \
   /            \
   1            14

n = 12
```             

**Output**

13 