package bst.closestvalue;

/***
 * Solution:
 * While iterating through the nodes of BST, we need to calculate difference in current closest with target and current node with target
 * Assign closest to which soever is less.
 * Once assigned, compare the target value with left and right node and move accordingly.
 */
public class ClosestValueInTree {

    public static void main(String[] args) {
        BST root = new BST(10);
        root.left = new BST(5);
        root.right = new BST(15);
        root.left.left = new BST(2);
        root.left.right = new BST(5);
        root.left.left.left = new BST(1);
        root.right.left = new BST(13);
        root.right.right = new BST(22);
        root.right.left.right = new BST(14);
        int target = 12;

        System.out.println("Nearest node to "+target+" has value: "+getClosestValue(root, root.value, target));
    }

    public static int getClosestValue(BST node, int closest, int target) {
        closest = Math.abs(target - closest) < Math.abs(target - node.value) ? closest : node.value;
        if (target < node.value && node.left != null) {
            return getClosestValue(node.left, closest, target);
        } else if (target > node.value && node.right != null) {
            return getClosestValue(node.right, closest, target);
        } else {
            return closest;
        }
    }

    static class BST{
        int value;
        BST left;
        BST right;
        public BST(int value){
            this.value = value;
        }
    }
}
