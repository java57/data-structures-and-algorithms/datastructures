package math.palindromeNumber;

/**
 * Basic idea to solve this problem is to divide a number to half and then compare both halves.
 * For a number with even number of digits, we need to take care of that extra digit
 */
public class PalindromeNumber {

    public static void main(String[] args) {
        System.out.println(isPalindrome(121));
        System.out.println(isPalindrome(1221));
        System.out.println(isPalindrome(1231));
    }

    public static boolean isPalindrome(int n){
        if(n<10){
            return true;
        }

        int reversedHalf = 0;
        while (reversedHalf<n){
            reversedHalf = reversedHalf*10 + n%10;
            n=n/10;
        }

        return reversedHalf == n || reversedHalf/10==n;
    }
}
