#### Problem Statement
```
A function that takes a string as input and gives the length of last string in it.
```

##### Example
```
input: Hello World
output = 5

Explanation:
Length of string "World" is 5 

```             