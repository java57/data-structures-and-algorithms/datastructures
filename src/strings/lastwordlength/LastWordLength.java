package strings.lastwordlength;

/**
 * It can be solved in multiple ways.
 * 1. Split string on space, and get the length of string at last index
 * 2. Start from end of string, for each character increment output count and return when first space is found
 */
public class LastWordLength {
    public static void main(String[] args) {
        System.out.println(lengthOfLastWord("Hello World"));
        System.out.println(lengthOfLastWord(" "));
        System.out.println(anotherSol("Hello World"));
        System.out.println(anotherSol(" "));
    }

    // Less space
    public static int lengthOfLastWord(String s) {
        if(s==null){
            return 0;
        }
        s = s.trim();
        if(s.length() == 0){
            return 0;
        }
        int count = 0;
        for(int i=s.length()-1;i>=0;i--){
            if(s.charAt(i)==' '){
                break;
            }
            count+=1;
        }
        return count;
    }

    // More space required
    public static int anotherSol(String s){
        if(s==null || s.length()==0){
            return 0;
        }
        String[] strings = s.split(" ");
        if(strings.length == 0){
            return 0;
        }
        return strings[strings.length-1].length();
    }
}
