package strings.strStr;

/**
 * This problem can be solved using substr method of String.
 * However, to make it more interesting I've used dynamic programming.
 * While comparing character in needle, if a match found, check diagonally up character.
 * If that matches too, then mark current character as matching
 */
public class ImplementStrStr {
    public static void main(String[] args) {
        System.out.println(strStr("banana", "na"));
        System.out.println(strStr("solution", "to"));
        System.out.println(strStr("mississippi", "pi"));
    }

    public static int strStr(String haystack, String needle){
        if(needle.length() == 0){
            return 0;
        }
        if(needle.length() > haystack.length()){
            return -1;
        }
        int row = needle.length()+1, col = haystack.length()+1;
        boolean[][] check = new boolean[row][col];
        for(int i=0;i<col;i++){
            check[0][i] = true;
        }

        for(int i=1;i<row;i++){
            for(int j=1;j<col;j++){
                if(needle.charAt(i-1)==haystack.charAt(j-1)){
                    if(check[i-1][j-1]){
                        check[i][j] = true;
                    }
                }

            }
        }

        int output = -1;
        for(int i=col-1; i>0;i--){
            if(check[row-1][i]){
                output = i;
            }
        }
        if(output==-1){
            return -1;
        }

        int endx = row-1, endy = output;
        while (true){
            if(endx == 1){
                return endy-1;
            }
            endx-=1;
            endy-=1;
        }
    }
}
