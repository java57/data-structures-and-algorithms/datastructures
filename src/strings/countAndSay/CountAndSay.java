package strings.countAndSay;

import java.sql.SQLOutput;

public class CountAndSay {
    public static void main(String[] args) {
        System.out.println(countAndSay(1));
        System.out.println(countAndSay(2));
        System.out.println(countAndSay(3));
        System.out.println(countAndSay(4));
        System.out.println(countAndSay(5));
        System.out.println(countAndSay(6));
    }

    public static String countAndSay(int n){
        if(n==1){
            return "1";
        }
        StringBuilder output = new StringBuilder("1");
        while (n>1){
            n-=1;
            output = readNumber(output);
        }
        return output.toString();
    }

    public static StringBuilder readNumber(StringBuilder input){
        StringBuilder output = new StringBuilder();
        int numCount = 0;
        Character check = null;
        for(int i=0;i<input.length();i++){
            if(numCount == 0){
                check = input.charAt(i);
                numCount+=1;
            }
            else{
                if(input.charAt(i)==check){
                    numCount+=1;
                }
                else{
                    output.append(numCount).append(check);
                    check = input.charAt(i);
                    numCount=1;
                }
            }
        }
        output.append(numCount).append(check);
        return output;
    }

}
