package strings.validparenthesis;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * To check last valid open bracket based on closed bracket, we can use stack very effectively.
 */
public class ValidParenthesis {
    public static void main(String[] args) {
        System.out.println(validateParenthesis("(){}"));
        System.out.println(validateParenthesis("({)}"));
        System.out.println(validateParenthesis("{}}"));
    }
    public static boolean validateParenthesis(String s){
        Stack<Character> openBrackets = new Stack<Character>();
        Character toCheck;
        Map<Character, Character> brackets = new HashMap<>();
        brackets.put('}','{');
        brackets.put(']','[');
        brackets.put(')','(');

        for(int i=0;i<s.length();i++){
            toCheck = s.charAt(i);
            switch (toCheck){
                case '{':
                case '[':
                case '(':
                    openBrackets.push(toCheck);
                    break;
                case '}':
                case ']':
                case ')':
                    if(openBrackets.size() == 0 || openBrackets.pop() != brackets.get(toCheck)){
                        return false;
                    }
            }
        }

        return openBrackets.size() == 0;
    }
}
