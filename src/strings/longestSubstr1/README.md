#### Problem Statement
```
A function that takes a string as input and gives maximum substring length, 
having atleast 1 upper case character and no digit
If there's no such substring, return 0
```

##### Example
```
input: abba
output = 0

Explanation:
In this input, there's no upper case character which is why output is 0


input: abBa0aaAaba
output = 6

Explanation:
For this input, there are two valid substrings, abBa and aaAaba. However, aaAaba is the longest one which is why output is 6
```             