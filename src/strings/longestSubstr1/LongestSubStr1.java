package strings.longestSubstr1;

/**
 * Basically, we have to keep track of a substring till the time we come to end or we encounter any digit.
 * After which we simply have to check if there's any upper case character in this substring or not.
 * If present, compare the last longest substring value and update it if required.
 */
public class LongestSubStr1 {

    public static void main(String[] args) {
        System.out.println(getLongestSubstr("abba"));
        System.out.println(getLongestSubstr("abBa"));
        System.out.println(getLongestSubstr("abBa0aAaaaa"));
        System.out.println(getLongestSubstr("abBa0aaaaaa"));
    }

    public static int getLongestSubstr(String input){
        int i=0,j=0, output = 0;
        char tempChar;
        boolean capsCheck = false;
        while(i<input.length()){
            tempChar = input.charAt(i);
            if(tempChar>='A' && tempChar<='Z'){
                capsCheck = true;
            }
            if(tempChar >='0' && tempChar <= '9'){
                if(capsCheck && output<i-j){
                    output = i-j;
                }
                j=i+1;
                capsCheck = false;
            }
            i+=1;
        }
        if(capsCheck){
            output = Math.max(output, i-j);
        }

        return output;
    }

}
