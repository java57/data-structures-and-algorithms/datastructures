#### Problem Statement
```
A function that takes 1-d array in input and we have to find a sub-array having max sum of it's digits.  
```

##### Example
```

input = [-2,1,-3,4,-1,2,1,-5,4]
output = 6
explanation: Sub array [4, -1, 2, 1] would have max sum which is 6

```       