package dp.maxsubarray;

/**
 * A classic dp problem where main idea is to find max data from current index value or (current index value + previous max)
 */
public class MaximumSubArray {
    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }

    public static int maxSubArray(int[] nums) {
        if(nums.length == 0){
            return 0;
        }
        if (nums.length == 1){
            return nums[0];
        }

        int output = nums[0];
        for(int i=1;i<nums.length;i++){
            nums[i] = Math.max(nums[i], nums[i]+nums[i-1]);
            output = Math.max(nums[i], output);
        }
        return output;
    }
}
