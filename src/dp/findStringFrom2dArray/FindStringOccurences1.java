package dp.findStringFrom2dArray;

/***
 * Solution: This is a very good DP problem.
 * As we know, we need to break the things in DP and calculate current answer based on previously calculated solution
 * In this case we will start from first value in array and check if string till that index is equal to the one we are checking.
 * Note that we have to check horizontal, vertical and diagonal values, which is why I've created a 3d matrix.
 * 0 - string at left
 * 1 - string at diagonal
 * 2 - string at top
 * Another thing to note, once, at current index, string length is equal to searching string length, we will simply remove first character
 * of string and continue our computation
 */
public class FindStringOccurences1 {

    public static void main(String[] args) {

        //char[][] words = {{'s','o','s','o'}, {'s','o','o','s'},{'s','s','s','s'}};
        //String str = "sos";
        char[][] words = {{'a','a'}, {'a','a'}};
        String str = "aa";
        System.out.println(findStringOccurences(words, str));
    }

    public static int findStringOccurences(char[][] incomingWords, String str){
        int occ = 0;

        StringBuilder[][][] subStrings = new StringBuilder[incomingWords.length+1][incomingWords[0].length+1][3];
        for(int i=0;i<subStrings.length;i++){
            for (int j=0;j<subStrings[i].length;j++){
                if(i==0 || j==0){
                    subStrings[i][j][0] = new StringBuilder("");
                    subStrings[i][j][1] = new StringBuilder("");
                    subStrings[i][j][2] = new StringBuilder("");
                }
            }
        }

        for(int i=1;i<subStrings.length;i++){
            for (int j=1;j<subStrings[i].length;j++){
                subStrings[i][j][0] = subStrings[i][j-1][0].append(incomingWords[i-1][j-1]);
                subStrings[i][j][1] = subStrings[i-1][j-1][1].append(incomingWords[i-1][j-1]);
                subStrings[i][j][2] = subStrings[i-1][j][2].append(incomingWords[i-1][j-1]);

                if(str.equalsIgnoreCase(subStrings[i][j][0].toString())){
                    occ+=1;
                }
                if(str.equalsIgnoreCase(subStrings[i][j][1].toString())){
                    occ+=1;
                }
                if(str.equalsIgnoreCase(subStrings[i][j][2].toString())){
                    occ+=1;
                }

                if(subStrings[i][j][0].length() == str.length()){
                    subStrings[i][j][0] = subStrings[i][j][0].deleteCharAt(0);
                }
                if(subStrings[i][j][1].length() == str.length()){
                    subStrings[i][j][1] = subStrings[i][j][1].deleteCharAt(0);
                }
                if(subStrings[i][j][2].length() == str.length()){
                    subStrings[i][j][2] = subStrings[i][j][2].deleteCharAt(0);
                }
            }
        }

        return occ;
    }
}
