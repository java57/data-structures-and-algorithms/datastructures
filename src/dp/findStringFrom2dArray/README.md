#### Problem Statement

A function that takes a 2-d array of characters and a string.
We are required to find number of occurrences of this string in the input array. 


##### Example
```
s   a   s   a
s   a   s   s
s   s   s   a
s   s   a   s

str = sas
```             

**Output**

5

2 strings(horizontal, diagonal) at [0,0], 1 string(horizontal) at [1,0], 1 string(vertical) at [1,3]
and 1 string(horizontal) at [3,1]
