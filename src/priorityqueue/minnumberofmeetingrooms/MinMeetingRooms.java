package priorityqueue.minnumberofmeetingrooms;

import java.util.*;

/***
 * Solution:
 * We simply need to see, when a meeting starts is there any ongoing meeting or not
 * We need to compare the other meeting's end time with current one.
 * For this we would be using Priority queue(as this works on min/max heap). This would help us to compare the first meeting about to finish.
 */
public class MinMeetingRooms {

    public static void main(String[] args) {

        List<Meeting> meetings = new ArrayList<>();
        meetings.add(new Meeting(0, 30));
        meetings.add(new Meeting(5, 10));
        meetings.add(new Meeting(15, 20));

        System.out.println(getMinMeetingRoomsRequired(meetings));
    }

    public static int getMinMeetingRoomsRequired(List<Meeting> meetings){
        int min = 0;

        meetings.sort(Comparator.comparingInt(o -> o.start));
        PriorityQueue<Meeting> ongoingMeeting = new PriorityQueue<>(Comparator.comparingInt(o->o.end));

        for (Meeting meeting : meetings) {
            while(ongoingMeeting.size()!=0 && meeting.start>ongoingMeeting.peek().end){
                ongoingMeeting.poll();
            }
            ongoingMeeting.add(meeting);
            min = Math.max(min, ongoingMeeting.size());
        }

        return min;
    }

    static class Meeting{
        int start;
        int end;
        Meeting(int start, int end){
            this.start = start;
            this.end = end;
        }
    }
}
