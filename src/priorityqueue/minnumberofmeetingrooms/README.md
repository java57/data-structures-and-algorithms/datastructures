#### Problem Statement

A function that takes a list of meetings containing (start, end) time.
We need to tell the minimum number of meeting rooms required to handle all meetings.
Input would be a list of Meeting. Meeting structure is as below.

```
class Meeting{
    int start;
    int end;
    public Meeting(int start, int end){
        this.start = start;
        this.end = end;
    }
}
```


##### Example
```
{2, 4}, {7, 9}, {3, 6}
```             

**Output**

2


If you observe, we have one meeting starting at 2 and ending at 4, which collides with another meeting starting at 3 till 6

However meeting starting at 7 doesn't collide with any other meeting. That's why minimum number of rooms required are 2.
